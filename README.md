# pk2cmd is a COMMAND LINE INTERFACE for PICkit 2

----
## Copyright ##

*This is a fork of **pk2cmd**, discontinued by [Microchip](http://www.microchip.com/DevelopmentTools/ProductDetails.aspx?PartNO=pg164120).*

----
## Dependencies ##

*Install required **libusb** library.*

*Fedora*

    $ sudo dnf install libusb*

*Debian*

    $ sudo apt-get install libusb-dev libusb-1.0-0-dev libudev-dev

----
## Setup ##

*Unpack and compile pk2cmd*

    $ make linux
    $ mkdir bin
    $ cp pk2cmd ./bin/

*Unpack PK2DeviceFile.zip*

    $ unzip PK2DeviceFile.zip
    $ cp PK2DeviceFile.dat ./bin/

**Attention!** *PK2DeviceFile.dat must be in the same directory than pk2cmd binary file.*

----
## Usage (as root!)##

*Binary file is in ./bin/*

    $ cd ./bin/

*Usage:*

    $ sudo ./pk2cmd <options>

### Verify that you can reach your PICkit 2 dongle ###

*First **connect** your PICkit 2 dongle.*

    $ sudo ./pk2cmd -?V

    Executable Version:    1.20.00
    Device File Version:   1.55.00
    OS Firmware Version:   2.32.00

    Operation Succeeded

*If you get something like:*

    OS Firmware Version:   PICkit 2 not found 

*Then make sure that the PICKit2 is connected and that you are running as root.*

    $ lsusb | grep Microchip
    Bus 003 Device 024: ID 04d8:0033 Microchip Technology, Inc. PICkit2

### Run auto-detection ###

    -P    tells pk2cmd which kind PIC we are programming.

----
    $ sudo ./pk2cmd -P
    Auto-Detect: Found part PIC18F4550.

    Operation Succeeded

*Now we know which PIC we have connected.*

### Erase the PIC ###

    -X    tells the PICKit2 to "Use VPP first Program Entry Method".
    -E    tells pk2cmd to erase the connected PIC.

----
    $ sudo ./pk2cmd -P PIC18F4550 -X -E
    Erasing Device...

    Operation Succeeded

### Program a HEX file to the PIC ###

    -M    tells pk2cmd to actually program the PIC.
    -MP   to program only Program memory.
    -ME   to program only EEPROM.
    -MI   to program only ID memory.
    -MC   to program only Configuration memory.
    -F    tells pk2cmd which file to use.

----
    $ sudo ./pk2cmd -P PIC18F4550 -X -M -F code.hex
    PICkit 2 Program Report
    16-8-2012, 10:21:05
    Device Type: PIC18F4550

    Program Succeeded.

    Operation Succeeded

### Verify a program ###

    -Y    tells pk2cmd to verify the PIC's memory with the HEX file given by -F.
    -YP   to verify only Program memory.
    -YE   to verify only EEPROM.
    -YI   to verify only ID memory.
    -YC   to verify only Configuration memory.

----
    $ sudo ./pk2cmd -P PIC18F4550 -Y -F code.hex
    PICkit 2 Verify Report
    16-8-2012, 10:28:59
    Device Type: PIC18F4550

    Verify Succeeded.

### Power ON ###

    $ pk2cmd -P PIC18F4550 -T
    Operation Succeeded

### Power OFF ###

    $ pk2cmd -P PIC18F4550
    Operation Succeeded